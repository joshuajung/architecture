=====================
Architecture Overview
=====================
Automotive software is increasingly complex as more of the final product is expressed in software. This complexity is found at many levels. Automotive OEMs have stated that the cost of developing board support packages and new System on Chip (SoC)/platform integration is unnecessary and very much a non-value added expense. SOAFEE intends to change this by adopting standards in both cloud native and edge computing allowing automotive OEMs to focus on their core competencies and increase the reusability of software. SOAFEE will adopt and enhance current standards used in the cloud-native world today to help manage the software and hardware complexities of the automotive software defined vehicle architecture.

SOAFEE Architecture Overview 
----------------------------
The primary objective of SOAFEE is to define a framework that supports cloud-native development and vehicle edge platform deployment of vehicle applications and functions. The framework allows to integrate different middleware and application Software stacks and focuses on the essential elements for building service oriented architectures in Automotive Use Cases. In addition, the SOAFEE architecture will enable cloud-native development of all workloads including those with functional safety, security, temporal partitioning, spatial partitioning and real-time requirements. These are important characteristics of next generation in-vehicle embedded edge platforms.

SOAFEE Architecture Guiding Principles
--------------------------------------
It is essential that real-world, deployable automotive use-cases drive the system architecture requirements for SOAFEE. The SOAFEE System Architecture Working Group (WG) has established a workflow by which automotive use-cases drive the architecture analysis, refinement, and allocation of requirements to the SOAFEE architecture planning process. Long-term, it is the goal of the SOAFEE System Arch WG to analyze and decompose all proposed use-cases to ensure that use-case requirements in the following areas are addressed by the SOAFEE System Architecture:

Safety 
~~~~~~
It is fully expected that the SOAFEE architecture will support use-cases that execute safety-critical (micro)services alongside non-safety-critical ones. Therefore, the execution platform needs to be reliable and safe to ensure the functional safety of such electrical/electronic (E/E) systems. The application of the ISO 26262 safety standard in the automotive domain helps to ensure the correct functioning of the safety-critical platform elements and to keep the residual errors acceptably low. 
Since the development of the entire platform compliant to a safety standard is not practical, the strategy is to develop (or qualify) only safety-critical elements according to ISO 26262 and isolate them from the non-safety-critical elements accordingly to ensure spatial, temporal and communication Freedom From Interference (FFI). 

Security 
~~~~~~~~
Security analysis of all use-case must be a common practice. All implementations shall pass security checks and follow a set of best practices, including:

#. Threat model: Platform and Applications. A threat model for the platform is to be created. All application decomposition should be associated with a bare minimal threat model.
#. Design principle: Principle of least privilege to be followed for the infrastructure (container runtime, orchestrator etc) and the applications.
#. Support of common security services such, but not limited to cryptography, attestation, etc. 
#. Robust security APIs to be used by applications. SOAFEE should include a standardized API. Not to define its own but use available standards.
#. Architecture agnostic deployment. It should be possible for applications to consume security services from the platform independent of how they are supported (SW, HSM, TEE etc.).
#. Discoverability of security services and their robustness. E.g., an application may be deployed only if the underlying HW provides crypto service from HSM.
#. Application authentications: How do we verify a deployed container is trusted? How do we verify a container when starting is not modified in storage?
#. Coding guidelines and static analysis. Propose that SOAFEE platform is to follow CERT coding guidelines (where applicable) and run static analysis to avoid vulnerabilities.
#. Security verifications: A basic minimal set of testing covering privilege checks, fuzzing. 

Real-Time 
~~~~~~~~~
SOAFEE is targeting use-cases that will require time constraints on the execution of the workloads. Real-time requirements must be considered from the point where workloads are analyzed and decomposed, throughout the design of the SOAFEE software stack, and finally when considering software / hardware interactions.
